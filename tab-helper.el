;;; tab-helper.el --- Additional functions and extensions for tab-bar.el  -*- lexical-binding: t -*-

;; Author: Robert Cochran <robert-git@cochranmail.com>.
;; URL: https://gitlab.com/RobertCochran/tab-helper
;; Version: 1.0.0
;; Package-Requires: ((emacs "27"))

;; Copyright (C) 2019 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides htop-like graphs for CPU and memory consumption.  A good
;; companion for proced.  Run `neato-graph-bar'.

;;; Code:

(require 'cl-lib)

(defun tab-helper--current-tab-p (tab)
  "Non-nil if TAB is the current tab."
  (eq (car tab) 'current-tab))

(defun tab-helper--read-frame-name (prompt)
  "Interactively select a frame by name, with PROMPT."
  (let ((frames (frame-list)))
    (cl-flet ((frame-name (f) (frame-parameter f 'name)))
      (cl-find (completing-read prompt
                                (mapcar #'frame-name
                                        (cl-remove (selected-frame) frames)))
               frames
               :key #'frame-name
               :test #'string=))))

(defun tab-helper-split-tab (tab-index)
  "Split the tab at TAB-INDEX into a new frame.
TAB-INDEX is 1-based."
  (interactive "P")
  ;; Only a user-error because the end result would have been the same anyways,
  ;; just let the user know.
  (when (= 1 (length (tab-bar-tabs)))
    (signal 'user-error '("Attempt to split the sole tab in a frame")))
  (let* ((tabs (tab-bar-tabs))
         (tab (nth (if tab-index
                       (1- (car tab-index))
                     (tab-bar--current-tab-index))
                   tabs))
         (tab-data (if (tab-helper--current-tab-p tab)
                       (tab-bar--tab)
                     tab))
         (wc (assq 'wc tab-data)))
    (tab-bar-close-tab tab-index)
    (select-frame (make-frame `((tabs (current-tab ,(assq 'name tab-data)
                                                   ,(assq 'explicit-name tab-data))))))
    (if (window-configuration-p wc)
        (set-window-configuration wc)
      (window-state-put (cdr (assq 'ws tab-data))
                        (frame-root-window (selected-frame))
                        'safe))
    (unless tab-bar-mode
      (message "Split tab '%s' into new frame" (cdr (assq 'name tab-data))))))

(defun tab-helper-join-tab (tab-index frame)
  "Join the tab at TAB-INDEX to FRAME.
Interactively, prompts for the name of the frame to join to.
TAB-INDEX is 1-based."
  (interactive (list (if (null current-prefix-arg)
                         (tab-bar--current-tab-index)
                       (car current-prefix-arg))
                     (tab-helper--read-frame-name "Destination frame: ")))
  (let* ((tabs (tab-bar-tabs))
         (tab (nth (1- tab-index) tabs))
         ;; Apparently window-configurations are per-frame in such a way that if
         ;; you try to transplate a tab directly into an existing frame, you're
         ;; going to have a bad time. As a result, strip out the
         ;; window-configuration and just keep the window-state when
         ;; moving. TODO: what about window-configurations are preventing this,
         ;; and what, if anything, are we losing by having to throw away the
         ;; window-configuration?
         (tab-data (cons 'tab
                         (cl-remove 'wc
                                    (cdr (if (tab-helper--current-tab-p tab)
                                             (tab-bar--tab)
                                           tab))
                                    :key #'car))))
    (if (= 1 (length (tab-bar-tabs)))
        (delete-frame)
      (tab-bar-close-tab-by-name (cdr (assq 'name tab-data))))
    (setf (frame-parameter frame 'tabs)
          (append (frame-parameter frame 'tabs)
                  (list tab-data)))
    (if tab-bar-mode
        (force-mode-line-update)
      (message "Joined tab '%s' to frame '%s'"
               (cdr (assq 'name tab-data))
               (cdr (frame-parameter frame 'name))))))

(defun tab-helper-duplicate-tab (&optional tab-index)
  "Make an identical copy of the tab at TAB-INDEX.
TAB-INDEX is 1-based."
  (interactive "p")
  (let* ((tab (nth (if tab-index
                       (1- tab-index)
                     (tab-bar--current-tab-index))
                   (tab-bar-tabs)))
         (tab-data (if (tab-helper--current-tab-p tab)
                      (tab-bar--tab)
                     tab)))
    (setf (frame-parameter nil 'tabs)
          (append (frame-parameter nil 'tabs)
                  (list tab-data)))
    (if tab-bar-mode
        (force-mode-line-update)
      (message "Duplicated tab '%s'" (cdr (assq 'name tab-data))))))

(provide 'tab-helper)

;;; tab-helper.el ends here
